from scipy.interpolate import make_interp_spline, BSpline, interpolate
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d


def accum(arr):
    sum = 0
    res = []

    for i in arr:
        sum = sum + i
        res.append(sum)

    return res


def plot_all(months, plots, label):

    x = np.array(months)
    y = np.array(accum(plots))

    x_new = np.linspace(x.min(), x.max(), 500)

    f = interp1d(x, y, kind='linear')
    y_smooth = f(x_new)

    plt.plot(x_new, y_smooth, )
    plt.scatter(x, y, label=label)


##### CHART FOR TASK 5a #######
task_1_2 = 1
task_1_3 = 2
task_1_4 = 1.5
task_1_5 = 2

task_2_1 = 4.5
task_2_2 = 2

task_3_1 = 25
task_3_2 = 28
task_3_3 = 16
task_3_4 = 33
task_3_5 = 12

task_4_1 = 670
task_4_2 = 450

task_5_1 = 875
task_5_2 = 18
task_5_3 = 120

task_6_1 = 75
task_6_2 = 55
task_6_3 = 90
task_6_4 = 8

task_7_1 = 210
task_7_2 = 250

task_8_1 = 190
task_8_2 = 170
task_8_3 = 220

task_9_1 = 25
task_9_2 = 4



months = [1, 5, 8, 12, 19, 22, 28, 34, 40, 54, 60, 64, 68, 71, 73, 77, 80, 82.5, 84, 85]
plots = [
    0,  # 1
    task_1_2,  # 5
    task_1_3 + task_1_4,  # 8
    task_1_5,  # 12
    task_2_1,  # 19
    task_2_2,  # 22
    (task_3_1 + (task_5_1 / 2)),  # 28
    task_3_2 + task_3_3 + task_3_4 + task_3_5 + (task_5_1 / 2),  # 34
    task_4_1,  # 40
    task_4_2 + task_5_2,  # 54
    task_5_3,  # 60
    task_6_1 + (task_6_2 / 2),  # 64,
    (task_6_2 / 2),  # 68
    task_6_3,  # 71
    task_6_4,  # 73
    task_7_1 + ( (4 * task_7_2) / 5 ), #77,
    ( (task_7_2) / 5 ) + task_8_1 + task_8_2,  # 80
    task_8_3,  # 82.5
    task_9_1,  # 84
    task_9_2,  # 85

]

plot_all(months, plots, "Budgeted cost (M$)")


##### CHART FOR TASK 5b #######
task_1_2 = 1
task_1_3 = 1.5
task_1_4 = 2.5
task_1_5 = 3

task_2_1 = 8
task_2_2 = 3

task_3_1 = 30
task_3_2 = 34
task_3_3 = 21
task_3_4 = 34
task_3_5 = 16

task_4_1 = 710
task_4_2 = 330

task_5_1 = 550
task_5_2 = 12
task_5_3 = 50

task_6_1 = 12
task_6_2 = 5
task_6_3 = 20
task_6_4 = 0

task_7_1 = 40
task_7_2 = 30

task_8_1 = 10
task_8_2 = 0
task_8_3 = 0

task_9_1 = 0
task_9_2 = 0

months = [1, 5, 8, 12, 19, 22, 28, 34, 40, 54, 62]
plots = [
    0,  # 1
    task_1_2,  # 5
    task_1_3 + task_1_4,  # 8
    task_1_5,  # 12
    task_2_1,  # 19
    task_2_2,  # 22
    (task_3_1 + (task_5_1 / 2)),  # 28
    task_3_2 + task_3_3 + task_3_4 + task_3_5 + (task_5_1 / 2),  # 34
    task_4_1,  # 40
    task_4_2 + task_5_2,  # 54
    task_5_3 + task_9_2 + task_9_1 + task_8_3 + task_7_1 + task_7_2 + task_8_1 + task_8_2 + task_6_4 + task_6_3 + task_6_1 + (task_6_2 / 2) + (task_6_2 / 2),  # 68

]

plot_all(months, plots, "Spent to date (M$)")

##### CHART FOR TASK 6c #######
task_1_2 = 1
task_1_3 = 2
task_1_4 = 1.5
task_1_5 = 2

task_2_1 = 4.5
task_2_2 = 2

task_3_1 = 25
task_3_2 = 28
task_3_3 = 16
task_3_4 = 29.7
task_3_5 = 10.2

task_4_1 = 603
task_4_2 = 337.5

task_5_1 = 568.75
task_5_2 = 3.6
task_5_3 = 48

task_6_1 = 15
task_6_2 = 2.75
task_6_3 = 9
task_6_4 = 0

task_7_1 = 42
task_7_2 = 62.5

task_8_1 = 19
task_8_2 = 0
task_8_3 = 0

task_9_1 = 0
task_9_2 = 0

months = [1, 5, 8, 12, 19, 22, 28, 34, 40, 54, 62]
plots = [
    0,  # 1
    task_1_2,  # 5
    task_1_3 + task_1_4,  # 8
    task_1_5,  # 12
    task_2_1,  # 19
    task_2_2,  # 22
    (task_3_1 + (task_5_1 / 2)),  # 28
    task_3_2 + task_3_3 + task_3_4 + task_3_5 + (task_5_1 / 2),  # 34
    task_4_1,  # 40
    task_4_2 + task_5_2,  # 54
    task_5_3 + task_9_2 + task_9_1 + task_8_3 + task_7_1 + task_7_2 + task_8_1 + task_8_2 + task_6_4 + task_6_3 + task_6_1 + (task_6_2 / 2) + (task_6_2 / 2),  # 68

]

plot_all(months, plots, "Earned Value (Proportional)")


plt.legend()

plt.ylabel('Costs (M$)', fontsize=18)
plt.xlabel('Months', fontsize=16)

plt.axvline(x=62)

plt.show()
